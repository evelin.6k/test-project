const gulp = require('gulp');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('clean', function () {
    return gulp.src('public/dist/*', {read: false})
    .pipe(clean());
});

gulp.task('buildScss', function () {
    return gulp.src(['public/src/scss/*.scss'])
    .pipe(sass())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(autoprefixer({
        env: ['last 2 versions'],
        cascade: false
        }))
    .pipe(concat("styles.css"))    
    .pipe(rename({suffix: '.min'}))
    .pipe(browserSync.stream())
    .pipe(gulp.dest('public/dist/'));
});

gulp.task('buildJs', function () {
        return gulp.src('public/src/js/*.js')
        .pipe(uglify())
        .pipe(concat("scripts.js"))
        .pipe(rename({suffix: '.min'}))
        .pipe(browserSync.stream())
        .pipe(gulp.dest('public/dist/'))
});

gulp.task('imageMin', exports.default = () => (
        gulp.src('public/src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/dist/img'))
));

gulp.task('dev', function () {
    browserSync.init({
        server: { baseDir: 'public/.'}
    })
    gulp.watch('public/src/scss/*.scss', gulp.series(['buildScss']))
    gulp.watch('public/src/js/*.js', gulp.series(['buildJs']))
    gulp.watch('public/index.html').on('change', () => browserSync.reload())
});

gulp.task('build', gulp.series('clean', gulp.parallel('buildScss', 'buildJs','imageMin')))